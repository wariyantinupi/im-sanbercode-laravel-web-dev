@extends('layouts.master')

@section('title')
    Halaman welcome
@endsection

@section('sub-title')
    welcome
@endsection

@section('content')
    <h1>Selamat Datang {{ $firstName }} {{ $lastName }}</h1>
    <h2>Terima kasih telah bergabung di Sanberbook/ Social Media kita Bersama</h2>
@endsection
