@extends('layouts.master')

@section('title')
    register
@endsection

@section('sub-title')
    register
@endsection

@section('content')
<form action="/send" method="post">
    @csrf
    <label>First Name</label><br><br>
    <input type="text" name="firstName"><br><br>
    <label>Last Name</label><br><br>
    <input type="text" name="lastName"><br><br>
    <label>Last Name</label><br><br>
    <input type="radio" name="male">Male<br>
    <input type="radio" name="female">Female<br>
    <input type="radio" name="otherGender">Other<br><br>
    <label>Nationality</label><br><br>
    <select name="nationality">
        <option value="indonesia">Indonesia</option>
        <option value="singapora">Singapura</option>
        <option value="afganistan">Afganistan</option>
    </select><br><br>
    <label>Language Spoken</label><br><br>
    <input type="checkbox" name="indo">Bahasa Indonesia<br>
    <input type="checkbox" name="eng">English<br>
    <input type="checkbox" name="otherLanguage">Other<br><br>
    <label>Bio</label><br><br>
    <textarea name="bio" rows="10"></textarea><br>
    <input type="submit" value="kirim">
</form>
@endsection

