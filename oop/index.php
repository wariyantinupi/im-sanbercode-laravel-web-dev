<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "name :". $sheep->name."<br>"; // "shaun"
echo "leg :".$sheep->legs."<br>"; // 4
echo "cold blooded :".$sheep->cold_blooded."<br>"; // "no"
echo "<br>";


// index.php


$kodok = new Frog("buduk");
$kodok->jump() ; // "hop hop"
echo "name :". $kodok->name."<br>"; // "shaun"
echo "leg :".$kodok->legs."<br>"; // 4
echo "cold blooded :".$kodok->cold_blooded."<br>"; // "no"
echo "Jump : ".$kodok->jump()."<br> <br>";



$sungokong = new Ape("kera sakti");
$sungokong->yell(); // "Auooo"
echo "name :". $sungokong->name."<br>"; // "shaun"
echo "leg :".$sungokong->legs."<br>"; // 4
echo "cold blooded :".$sungokong->cold_blooded."<br>"; // "no"
echo "Yell : ".$sungokong->yell()."<br> <br>";


