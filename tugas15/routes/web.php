<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//testing master template
Route::get('/master',function(){
    return view('layout.master');
});


//menampilkan halaman cast

Route::get('/cast', [CastController::class,'index']);

//menampilkan halaman tambah cast
Route::get('/cast/create', [CastController::class,'create']);

//mengambil data ari halaman tambah cast
Route::post('/cast/create', [CastController::class,'store']);

//menampilkan detail bio
Route::get('/cast/bio/{id}', [CastController::class,'show']);

//menampilkan halaman edit
Route::get('/cast/edit/{id}', [CastController::class,'edit']);

//mengedit data cast
Route::put('/cast/edit/{id}', [CastController::class,'update']);


//delete data cast
Route::delete('/cast/{id}', [CastController::class,'destroy']);
