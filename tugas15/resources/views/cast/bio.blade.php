@extends('layouts.master')

@section('title')
    Detail Cast
@endsection

@section('sub-title')
    {{ $cast->nama }}
@endsection

@section('content')
    {{ $cast->bio }}
    <br>
    <br>
    <a class="btn btn-secondary" href="/cast">Kembali</a>
@endsection
