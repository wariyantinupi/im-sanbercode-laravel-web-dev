@extends('layouts.master')

@section('title')
    Pemain
@endsection

@section('sub-title')
    Menambahkan Pemain
@endsection

@section('content')
    <form action="/cast/edit/{{$cast->id}}" method="post">
        @csrf
        @method('put')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{$cast->nama}}">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" placeholder="Umur" name="umur" value="{{$cast->umur}}">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea type="" class="form-control" placeholder="Bio" name="bio">{{$cast->bio}} </textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
        <a class="btn btn-secondary" href="/cast">Kembali</a>
    </form>
    

@endsection
