@extends('layouts.master')

@section('title')
    Pemain
@endsection

@section('sub-title')
    Daftar Pemain
@endsection

@section('content')
    <a class="btn btn-success" href="/cast/create">Tambah Pemain</a>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Aksi</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($cast as $key => $item)
                <tr>
                    <th>{{ $item->id }}</th>
                    <td>{{ $item->nama }}</td>
                    <td>{{ $item->umur }}</td>
                    <td>

                        <form action="/cast/{{$item->id}}" method="post">
                            @csrf
                            @method('delete')
                            <a class="btn btn-primary" href="/cast/bio/{{ $item->id }}">Lihat Bio</a>
                            <a class="btn btn-warning" href="/cast/edit/{{ $item->id }}">Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            @empty
                <tr>
                    <th scope="col">data kosong</th>
                </tr>
            @endforelse
        </tbody>
    </table>
@endsection
