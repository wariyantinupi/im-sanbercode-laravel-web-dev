@extends('layouts.master')

@section('title')
    Pemain
@endsection

@section('sub-title')
    Menambahkan Pemain
@endsection

@section('content')
    <form action="" method="post">
        @csrf
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control" placeholder="Nama" name="name">
            @error('nama')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control" placeholder="Umur" name="age">
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea type="" class="form-control" placeholder="Bio" name="bio"> </textarea>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <a class="btn btn-secondary" href="/cast">Kembali</a>
    </form>
    

@endsection
